package xperiment;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import xperiment.util.Random;

public abstract class Experiment<T> {

    public final long SEED;
    public final int NUM_RUNS;

    private final String name;
    private long startTime;
    private long endTime;

    protected T output;

    protected static final String EOL = "\r\n";

    public Experiment(String name, long seed, int numRuns) {
        this.name = name;
        this.SEED = seed;
        this.NUM_RUNS = numRuns;
    }

    public T startExperiment() {
        Random.getInstance().setSeed(SEED);
        startTime = System.currentTimeMillis();
        output = performExperiment();
        endTime = System.currentTimeMillis();
        return output;
    }

    protected abstract T performExperiment();

    protected String showOutput(T output) {
        return output.toString();
    }

    @Override
    public String toString() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
        return wrap("-----------------------------------------------------------")
                + EOL
                + wrap("Experiment: " + name)
                + EOL
                + String.join(EOL,
                        getParameters().stream().map(Experiment::wrap).collect(Collectors.toList()))
                + EOL 
                + wrap("Started at: " + format.format(new Date(startTime)))
                + EOL
                + wrap("Ended at: " + format.format(new Date(endTime)))
                + EOL
                + showOutput(output)
                + wrap("-----------------------------------------------------------");
    }

    protected List<String> getParameters() {
        List<String> params = new ArrayList<>();
        params.add("Seed: " + SEED);
        params.add("Runs: " + NUM_RUNS);
        return params;
    }

    public void toFile() throws IOException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

        File file = new File(String.format("%s-%s.txt", name.substring(0, 3), format.format(new Date(startTime))));
        String content = toString();
        try (FileWriter fw = new FileWriter(file)) {
            fw.write(content);
        }
        System.out.printf("(*%s:*) Get[\"%s\"]\r\n", name, file.getAbsolutePath().replace("\\", "\\\\"));
    }

    /**
     * Wrap the given string in a mathematica comment
     */
    public static String wrap(String str) {
        return "(*" + str + "*)";
    }

}
