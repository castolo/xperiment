package xperiment.stats;

public class Statistics {

    private double sum;
    private double sumSq;
    private double min;
    private double max;

    private double mean;
    private double sd;

    private int numSamples;
    private boolean dirty;

    public Statistics() {
        numSamples = 0;
        sum = 0;
        sumSq = 0;
        min = Double.POSITIVE_INFINITY;
        max = Double.NEGATIVE_INFINITY;
        dirty = true;
    }

    public final void clear() {
        numSamples = 0;
        sum = 0;
        sumSq = 0;
        min = Double.POSITIVE_INFINITY;
        max = Double.NEGATIVE_INFINITY;
    }

    public double max() {
        return max;
    }

    public double min() {
        return min;
    }

    public double mean() {
        if (dirty) {
            computeStats();
        }
        return mean;
    }

    public double sumSquareDiff() {
        return sumSq - numSamples * mean() * mean();
    }

    private void computeStats() {
        if (dirty) {
            mean = sum / numSamples;
            double num = sumSq - (numSamples * mean * mean);
            if (num < 0) {
                num = 0;
            }
            sd = Math.sqrt(num / (numSamples - 1));
            dirty = false;
        }
    }

    public double sd() {
        if (dirty) {
            computeStats();
        }
        return sd;
    }

    public int N() {
        return numSamples;
    }

    public double stdErr() {
        return sd() / Math.sqrt(numSamples - 1);
    }

    public void add(Statistics ss) {
        numSamples += ss.numSamples;
        sum += ss.sum;
        sumSq += ss.sumSq;
        max = Math.max(max, ss.max);
        min = Math.min(min, ss.min);
        dirty = true;
    }

    public void add(Number number) {
        double d = number.doubleValue();
        numSamples++;
        sum += d;
        sumSq += d * d;
        min = Math.min(min, d);
        max = Math.max(max, d);
        dirty = true;
    }

}
