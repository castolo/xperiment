package xperiment.plot;

import java.util.List;
import java.util.StringJoiner;
import xperiment.util.Triple;

/**
 *
 * @author Steve
 */
public class ShadedErrorBarPlotter extends Plotter3D {

    public ShadedErrorBarPlotter() {
    }

    public ShadedErrorBarPlotter(String title) {
        super(title);
    }


    @Override
    public String toString() {

        int nFunctions = map.size();

        StringJoiner legends = new StringJoiner(", ", "PlotLegends -> Placed[{", "},{1, 0.5}]");
        StringJoiner rows = new StringJoiner(",\n", "{\n", "\n}");
        for (String name : map.keySet()) {
            List<Triple<Double, Double, Double>> list = map.get(name);
            if (!list.isEmpty()) {
                legends.add("\"" + name + "\"");
                StringJoiner row = new StringJoiner(", ", "{", "}");
                list.stream().forEach((t) -> row.add(toString(t)));
                rows.add(row.toString());
            }
        }

        return String.format(FUNCTIONS, legends, getOptions()) + "\ndata = " + rows.toString() + ";\n\n"
                + getColours(nFunctions)
                + "\n\nShow@MapThread[ePlotFunc, {data, colors}]";

    }

    private final static String FUNCTIONS = "ClearAll[ePlot, ePlotFunc, plusMinusMean];\n"
            + "plusMinusMean[a_, b_] := {a + b, a - b, a};\n"
            + "ePlot[plotFun_, dataX_, plusMinList_, color_] := \n"
            + "  Block[{f}, f[y_] := Transpose[{dataX, y}];\n"
            + "    plotFun[{f[plusMinList[[All, 1]]], f[plusMinList[[All, 2]]], \n"
            + "        f[plusMinList[[All, 3]]]}, Filling -> {1 -> {2}}, \n"
            + "      Joined -> {True, True, True}, \n"
            + "      PlotStyle -> {Opacity[0], Opacity[0], Darker@color}, \n"
            + "      PlotMarkers -> {Graphics@{Disk[]}, 0.03}, \n"
            + "      FillingStyle -> Directive[Opacity[0.5], color],\n %s, \n"
            + "      %s]]\n"
            + "\n"
            + "\n"
            + "ePlotFunc[data_, color_] := \n"
            + "  Module[{dataY, dataX, errorY, plusMinList}, dataY = data[[All, 2]];\n"
            + "    dataX = data[[All, 1]];\n"
            + "    errorY = data[[All, 3]];\n"
            + "    plusMinList = Thread[plusMinusMean[dataY, errorY]];\n"
            + "    ePlot[ListPlot, dataX, plusMinList, color]]\n";

    private String getColours(int nFunctions) {

        return "colors = {Hue[0.5991165296252574, 0.48095514498491126, 0.709798], Hue[0.10581842254535508, 0.8387107395977391, 0.880722], Hue[0.21075506089720356, 0.7181987625240577, 0.691569], \n"
                + " Hue[0.041225144751899624, 0.7732540871476793, 0.922526], Hue[0.7084649824251171, 0.3289750780992684, 0.701351], Hue[0.0819199970533718, 0.8673879227384762, 0.772079], \n"
                + " Hue[0.5652597317248614, 0.5348648748832042, 0.782349], Hue[0.125, 1., 1.], Hue[0.8541072895328009, 0.416080935851667, 0.647624], Hue[0.1708992417512528, 1., 0.586483], \n"
                + " Hue[0.02846975088967972, 0.76775956284153, 0.915], Hue[0.6217013822105728, 0.5284444398899688, 0.85], Hue[0.10159396631650712, 0.9245890037393875, 0.9728288904374106], \n"
                + " Hue[0.9361873937107026, 0.5141036654876633, 0.736782672705901], Hue[0.3904349085688621, 0.6080218036685776, 0.715]}[[1;;" + nFunctions + "]];";

    }

}
