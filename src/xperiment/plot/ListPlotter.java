package xperiment.plot;

import xperiment.util.Triple;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.TreeMap;

/**
 *
 * @author Steve James <SD.James@outlook.com>
 */
public class ListPlotter extends Plotter {

    private static final String FORMATTER = "{%f, %f}";

    private final Map<String, List<Triple<Double, Double, Double>>> map = new TreeMap<>();

    public ListPlotter() {
        super(FORMATTER);
    }

    public ListPlotter(String title) {
        super(title, FORMATTER);
    }

    public ListPlotter(String title, String xLabel, String yLabel) {
        super(title, xLabel, yLabel, FORMATTER);
    }

    public void add(String name, double independentVariable, double mean, double stdErr) {
        List<Triple<Double, Double, Double>> list;
        if (map.containsKey(name)) {
            list = map.get(name);
        } else {
            list = new ArrayList<>();
        }
        list.add(Triple.of(independentVariable, mean, stdErr));
        map.put(name, list);
    }

    @Override
    public String toString() {
        StringJoiner legends = new StringJoiner(", ", "PlotLegends -> Placed[{", "},{1, 0.5}]");
        StringJoiner rows = new StringJoiner(",\n", "{\n", "\n}");
        for (String name : map.keySet()) {
            List<Triple<Double, Double, Double>> list = map.get(name);
            if (!list.isEmpty()) {
                legends.add("\"" + name + "\"");
                StringJoiner row = new StringJoiner(", ", "{", "}");
                list.stream().forEach((t) -> row.add(toString(t)));
                rows.add(row.toString());
            }
        }
        return String.format("ListPlot[%s, \n%s, \n%s]", rows.toString(), legends, getOptions());
    }

    @Override
    public String toString(Triple<?, ?, ?> triple) {
        return String.format(FORMATTER, triple.first, triple.second);
    }
    
    
}
