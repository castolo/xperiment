package xperiment.plot;

import xperiment.util.Triple;

/**
 *
 * @author Steve James <SD.James@outlook.com>
 */
public abstract class Plotter {

    protected String title = "Plot Title";
    protected String xAxisLabel = "x-axis";
    protected String yAxisLabel = "y-axis";
    protected String zAxisLabel = null;

    private final String elementFormatter;

    public Plotter() {
        this(null);
    }

    public Plotter(String elementFormatter) {
        this("Plot Title", "x-axis", "y-axis", elementFormatter);
    }

    public Plotter(String title, String elementFormatter) {
        this(title, "x-axis", "y-axis", elementFormatter);
    }

    public Plotter(String title, String xLabel, String yLabel, String elementFormatter) {
        this.title = title;
        xAxisLabel = xLabel;
        yAxisLabel = yLabel;
        this.elementFormatter = elementFormatter;
    }

    public Plotter(String title, String xLabel, String yLabel, String zLabel, String elementFormatter) {
        this.title = title;
        xAxisLabel = xLabel;
        yAxisLabel = yLabel;
        this.zAxisLabel = zLabel;
        this.elementFormatter = elementFormatter;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAxesLabels(String xLabel, String yLabel) {
        xAxisLabel = xLabel;
        yAxisLabel = yLabel;
    }

    public void setAxesLabels(String xLabel, String yLabel, String zLabel) {
        xAxisLabel = xLabel;
        yAxisLabel = yLabel;
        zAxisLabel = zLabel;
    }

    @Override
    public abstract String toString();

    protected String getOptions() {
        if (zAxisLabel != null) {
            return String.format("PlotLabel -> Style[\"%s\", FontSize -> 18],"
                    + " PlotRange -> All,  AxesLabel -> {\"%s\", \"%s\", \"%s\"},"
                    + " LabelStyle -> {FontSize -> 18}",
                    title, xAxisLabel, yAxisLabel, zAxisLabel);
        }
        return String.format("Joined -> True, PlotLabel -> Style[\"%s\", FontSize -> 18],"
                + " Frame -> {{True, False}, {True, False}}, PlotRange -> All,"
                + " FrameLabel -> {\"%s\", \"%s\"}, LabelStyle -> {FontSize -> 18}",
                title, xAxisLabel, yAxisLabel);
    }

    public String toString(Triple<?, ?, ?> triple) {
        return String.format(elementFormatter, triple.first, triple.second, triple.third);
    }

}
