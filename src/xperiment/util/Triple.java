/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xperiment.util;

import java.util.Objects;

/**
 *
 * @author Steve James <SD.James@outlook.com>
 */
public class Triple<S, T, U> {

    public final S first;
    public final T second;
    public final U third;

    private Triple(S first, T second, U third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public static <S, T, U> Triple<S, T, U> of(S first, T second, U third) {
        return new Triple<>(first, second, third);
    }

    @Override
    public String toString() {
        return String.format("{%s}", String.join(", ",
                first.toString(), second.toString(), third.toString()));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.first);
        hash = 79 * hash + Objects.hashCode(this.second);
        hash = 79 * hash + Objects.hashCode(this.third);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Triple<?, ?, ?> other = (Triple<?, ?, ?>) obj;
        if (!Objects.equals(this.first, other.first)) {
            return false;
        }
        if (!Objects.equals(this.second, other.second)) {
            return false;
        }
        return Objects.equals(this.third, other.third);
    }
    
    

}
