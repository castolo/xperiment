package xperiment.util;

/**
 * This class is the RNG for Java, but which acts as a singleton. To produce
 * repeatable experiments, set the seed of the singleton initially, and use it
 * everywhere a random number is required.
 *
 * @author Steve James <SD.James@outlook.com>
 */
public class Random extends java.util.Random {

    private static final Random instance = new Random();

    public static final double EPSILON = Math.ulp(1.0);

    private Random() {
    }

    public static Random getInstance() {
        return instance;
    }

    public double pick(double... options) {
        return options[nextInt(options.length)];
    }

    public int pick(int... options) {
        return options[nextInt(options.length)];
    }

    public float pick(float... options) {
        return options[nextInt(options.length)];
    }

    public <T> T pick(T... options) {
        return options[nextInt(options.length)];
    }

    public int nextInt(int lower, int upper) {
        return lower + nextInt(upper - lower);
    }

    public double nextGaussian(double mean, double stdDev) {
        return mean + (nextGaussian() * stdDev);
    }

    public int rouletteSelection(double... weightings) {
        double sum = 0.0D;
        for (double a : weightings) {
            assert a >= 0 : "Weights must be non-negative";
            sum += a;
        }
        double p = nextDouble() * sum;
        int i = 0;
        for (double weight : weightings) {
            p -= weight;
            if (p <= 0.0D) {
                return i;
            }
            i++;
        }
        return weightings.length - 1;
    }

    public int getOutcome(double winProbability) {

        if (nextDouble() <= winProbability) {
            return 1;
        }
        return 0;
    }

    public double nextDouble(double begin, double end) {
        return begin + (nextDouble() * (end - begin));
    }

    public double[] nextDoubleArray(int length) {
        return nextDoubleArray(length, 0, 1);
    }

    public double[] nextDoubleArray(int length, double begin, double end) {
        double[] x = new double[length];
        for (int i = 0; i < length; i++) {
            x[i] = nextDouble(begin, end);
        }
        return x;
    }

}
